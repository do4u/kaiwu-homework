# 题目要求：

1.在MySQL数据中建立lagou_db数据库, 将position.sql中的数据导入到mysql 数据中。

2.选择一种合理的方式(可以使用上课中使用的方式 也可以选择自己熟悉的插件等) 将mysql中的数据导入到ES中。

3.使用SpringBoot 访问ES 使用positionName 字段检索职位信息 如果检索到的职位信息不够5条 则需要

启用positionAdvantage 查找 美女多、员工福利好 的企业职位信息进行补充够5条。

提示:

   搜索一次查询不能满足要求时 发起第二次搜索请求。搜索一次满足需求不需要发送第二次搜索请求。

作业需求:

​    1.提交导入数据的过程视频 和 案例讲解视频

​    2.提交案例项目代码

sql文件：

[https://gitee.com/tian_lanlan/java_001/blob/master/%E4%BD%9C%E4%B8%9A%E8%B5%84%E6%96%99/position.sql](https://gitee.com/tian_lanlan/java_001/blob/master/作业资料/position.sql)



## 答题：

思路：采用spring-boo-data-elasticsearch导入和分页查询

导入：http://127.0.0.1:8080/import

查询：http://127.0.0.1:8080/query?keyWord=java&page=0&pageSize=10

视频演示：https://gitee.com/do4u/kaiwu-homework/blob/master/stage07/module02/homework/video.wmv

![image-20201218023107051](homework.assets/image-20201218023107051.png)