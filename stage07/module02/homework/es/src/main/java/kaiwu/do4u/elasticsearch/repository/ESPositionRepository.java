package kaiwu.do4u.elasticsearch.repository;


import kaiwu.do4u.elasticsearch.model.ESPosition;
import kaiwu.do4u.elasticsearch.model.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ESPositionRepository extends ElasticsearchRepository<ESPosition,String> {

    Page<ESPosition> findByPositionName(String positionName, Pageable pageable);

    Page<ESPosition> findByPositionAdvantage(String positionName, Pageable pageable);
}
