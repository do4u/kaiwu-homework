package kaiwu.do4u.elasticsearch.repository;


import kaiwu.do4u.elasticsearch.model.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DBPositionRepository extends JpaRepository<Position,String> {

}
