package kaiwu.do4u.elasticsearch.controller;

import kaiwu.do4u.elasticsearch.model.ESPosition;
import kaiwu.do4u.elasticsearch.service.ESService;
import kaiwu.do4u.elasticsearch.util.JsonResult;
import kaiwu.do4u.elasticsearch.util.JsonResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ESController {

    @Autowired
    ESService esService;


    /**
     * 导入
     * @return
     */
    @RequestMapping("/import")
    public JsonResult importDB(){
        esService.importES();
        return JsonResultUtil.success();
    }

    @RequestMapping("/query")
    public JsonResult query(String keyWord, Integer page , Integer pageSize){
        if(keyWord==null){
            return JsonResultUtil.fail("请输入查询条件");
        }
        if(page == null){
            page = 0;
        }
        if(pageSize == null){
            pageSize = 5;
        }
        List<ESPosition> by = esService.findBy(keyWord, PageRequest.of(page, pageSize));
        return JsonResultUtil.success(by);
    }


}
