package kaiwu.do4u.elasticsearch.service;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.log.LogFactory;
import cn.hutool.log.StaticLog;
import kaiwu.do4u.elasticsearch.model.ESPosition;
import kaiwu.do4u.elasticsearch.model.Position;
import kaiwu.do4u.elasticsearch.repository.DBPositionRepository;
import kaiwu.do4u.elasticsearch.repository.ESPositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ESService {

    @Autowired
    ESPositionRepository esPositionRepository;
    @Autowired
    DBPositionRepository dbPositionRepository;

    public List<ESPosition> findBy(String keyWord,PageRequest pageRequest){
        Page<ESPosition> byPositionName = esPositionRepository.findByPositionName(keyWord, pageRequest);
        List<ESPosition> content = byPositionName.getContent();
        if(content.size() < 5){
            Page<ESPosition> byPositionAdvantage = esPositionRepository.findByPositionAdvantage(keyWord, PageRequest.of(1, 5));
            List<ESPosition> content1 = byPositionAdvantage.getContent();
            if(content1.size() > 0){
                for(ESPosition esPosition: content1){
                    content.add(esPosition);
                    if(content.size() == 5){
                        break;
                    }
                }
            }
        }
        return content;
    }


    public void importES(){
        int page = 1;
        int pageSize = 10;
        long totalElements = -1;
        while (totalElements== -1 || totalElements > 0){
            StaticLog.info("分页：{}",page);
            Page<Position> all = dbPositionRepository.findAll(PageRequest.of(page, pageSize));
            if(page == 1){
                StaticLog.info("合计{}条记录,{}页",all.getTotalElements(),all.getTotalPages());
            }
            List<Position> content = all.getContent();
            List<ESPosition> list = ListUtil.list(false);
            for(Position position : content){
                ESPosition esPosition = BeanUtil.copyProperties(position, ESPosition.class);
                list.add(esPosition);
            }
            //批量插入ES
            Iterable<ESPosition> esPositions = esPositionRepository.saveAll(list);
            esPositions.forEach( esPosition -> StaticLog.info("ESposition {}" , esPosition));
            if(content.size() == 0){
                break;  //查询不出数据后退出循环
            }





            page++;
        }




    }

}
