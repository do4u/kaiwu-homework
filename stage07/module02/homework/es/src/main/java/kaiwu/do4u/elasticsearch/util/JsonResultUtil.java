package kaiwu.do4u.elasticsearch.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chenyu
 * @version 2018/9/6 15:26
 */
public class JsonResultUtil {

    public static JsonResult success(){
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(0);
        jsonResult.setMessage("OK");
        return jsonResult;
    }

    public static JsonResult success(Integer code, String msg,Object data){
        JsonResultExt jsonResult = new JsonResultExt();
        jsonResult.setCode(code);
        jsonResult.setMessage(msg);
        jsonResult.setData(data);
        return jsonResult;
    }

    public static JsonResult successList(String page,String pageSize, Object list){
        JsonResultExt jsonResult = new JsonResultExt();
        jsonResult.setCode(0);
        jsonResult.setMessage("OK");
        Map<String,Object> map = new HashMap<>();
        map.put("page",page);
        map.put("pageSize",pageSize);
        map.put("list",list);
        jsonResult.setData(map);
        return jsonResult;
    }

    public static JsonResult successList(Object list){
        JsonResultExt jsonResult = new JsonResultExt();
        jsonResult.setCode(0);
        jsonResult.setMessage("OK");
        Map<String,Object> map = new HashMap<>();
        map.put("list",list);
        jsonResult.setData(map);
        return jsonResult;
    }

    public static JsonResult success(Object data){
        JsonResultExt jsonResult = new JsonResultExt();
        jsonResult.setCode(0);
        jsonResult.setMessage("OK");
        jsonResult.setData(data);
        return jsonResult;
    }

    public static JsonResult fail(Integer code,String msg){
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(code);
        jsonResult.setMessage(msg);
        return jsonResult;
    }

    public static JsonResult fail(String msg){
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(-1);
        jsonResult.setMessage(msg);
        return jsonResult;
    }

    /*public static void fail(HttpServletResponse response,String msg) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().append("{\"code\": -1,\"message\":\""+msg+"\"}");
    }

    public static void success(HttpServletResponse response,String msg) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().append("{\"code\": 0,\"message\":\""+msg+"\"}");
    }

    public static void success(HttpServletResponse response,String msg,Object data) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        JsonResult jsonResult = JsonResultUtil.success(0,msg,data);
        response.getWriter().append(jsonResult.toJsonString());
    }*/
}
