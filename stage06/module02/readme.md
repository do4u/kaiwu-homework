## ****模块作业：****

 

使用Kafka做日志收集。

一、需要收集的信息：
1、用户ID（user_id）
2、时间（act_time）
3、操作（action，可以是：点击：click，收藏：job_collect，投简历：cv_send，上传简历：cv_upload）
4、对方企业编码（job_code）

二、工作流程：

![img](readme.assets/wps1.jpg) 
1、HTML可以理解为拉勾的职位浏览页面
2、用户的操作会由Web服务器进行响应。
3、同时用户的操作也会使用ajax向Nginx发送请求，nginx用于收集用户的点击数据流。
4、Nginx收集的日志数据使用ngx_kafka_module将数据发送到Kafka集群的主题中。
5、只要数据保存到Kafka集群主题，后续就可以使用大数据组件进行实时计算或其他的处理了，比如职位推荐，统计报表等。


三、架构：
HTML+Nginx+[ngx_kafka_module](https://github.com/brg-liuwei/ngx_kafka_module)+Kafka
ngx_kafka_module网址：https://github.com/brg-liuwei/ngx_kafka_module


注意问题：由于使用ngx_kafka_module，只能接收POST请求，同时一般Web服务器不会和数据收集的Nginx在同一个域名，会涉及到使用ajax发送请求的跨域问题，可以在nginx中配置跨域来解决。

 四、实战步骤：
1. 安装Kafka
2. 安装Nginx
3. 配置ngx_kafka_module，注意跨域配置
4. 开发HTML页面



## 答题：

安装视频：
单机安装：
https://gitee.com/do4u/kaiwu-homework/blob/master/stage06/module02/%E5%AE%89%E8%A3%85kafka.wmv

集群安装（录制内容过大无法直接上传，切割为三个小于100M文件）：
https://gitee.com/do4u/kaiwu-homework/blob/master/stage06/module02/kafka%E9%9B%86%E7%BE%A4%E6%90%AD%E5%BB%BA_edit_01.wmv

https://gitee.com/do4u/kaiwu-homework/blob/master/stage06/module02/kafka%E9%9B%86%E7%BE%A4%E6%90%AD%E5%BB%BA_edit_02.wmv

https://gitee.com/do4u/kaiwu-homework/blob/master/stage06/module02/kafka%E9%9B%86%E7%BE%A4%E6%90%AD%E5%BB%BA_edit_03.wmv

nginx安装：
https://gitee.com/do4u/kaiwu-homework/blob/master/stage06/module02/nginx%E6%90%AD%E5%BB%BA.wmv

1. 编辑4台主机hosts文件

```sh
#hosts文件
vim /etc/hosts
192.168.3.101 node1
192.168.3.102 node2
192.168.3.103 node3
192.168.3.104 node4
```
2. 监听ipv6导致无法连接，关闭
```sh
#关闭ipv6
vi /etc/sysctl.conf
#添加一下内容
net.ipv6.conf.all.disable_ipv6=1
net.ipv6.conf.default.disable_ipv6=1

sysctl -p #执行此命令生效
```

3. 安装jdk、zk、kafka配置对应环境变量 /etc/profile

```sh
export JAVA_HOME=/usr/java/jdk1.8.0_261-amd64
export PATH=$PATH:$JAVA_HOME/bin

export ZOOKEEPER_PREFIX=/opt/zookeeper-3.4.14
export PATH=$PATH:$ZOOKEEPER_PREFIX/bin
export ZOO_LOG_DIR=/var/kafka/zookeeper/log

export KAFKA_HOME=/opt/kafka_2.12-1.0.2
export PATH=$PATH:$KAFKA_HOME/bin
```
4. 安装依赖

   ```shell
   yum install wget git -y
   yum install gcc-c++ -y
   
   git clone https://github.com/edenhill/librdkafka
   cd librdkafka
   ./configure
   make
   sudo make install
   ```

5. 下载nginx

   ```shell
   wget http://nginx.org/download/nginx-1.17.8.tar.gz
   tar -zxf nginx-1.17.8.tar.gz
   cd nginx-1.17.8
   yum install gcc zlib zlib-devel openssl openssl-devel pcre pcre-devel -y
   ```

6. 下载ngx_kafka_module

   ```shell
   cd ~
   git clone https://github.com/brg-liuwei/ngx_kafka_module.git
   cd nginx-1.17.8
   ./configure --add-module=/root/ngx_kafka_module
   make
   sudo make install
   ```

7. 配置nginx：nginx.conf

   ```json
   #user  nobody;
   worker_processes  1;
   
   #error_log  logs/error.log;
   #error_log  logs/error.log  notice;
   #error_log  logs/error.log  info;
   
   #pid        logs/nginx.pid;
   
   
   events {
       worker_connections  1024;
   }
   
   
   http {
       include       mime.types;
       default_type  application/octet-stream;
   
       #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
       #                  '$status $body_bytes_sent "$http_referer" '
       #                  '"$http_user_agent" "$http_x_forwarded_for"';
   
       #access_log  logs/access.log  main;
   
       kafka;
   
       kafka_broker_list node2:9092 node3:9092 node4:9092;    
   
       sendfile        on;
       #tcp_nopush     on;
   
       #keepalive_timeout  0;
       keepalive_timeout  65;
   
       #gzip  on;
   
       server {
           listen       9000;
           server_name  localhost;
   
           #charset koi8-r;
   
           #access_log  logs/host.access.log  main;
           #
      		location = /log {
          		add_header 'Access-Control-Allow-Origin' $http_origin;
         		 add_header 'Access-Control-Allow-Credentials' 'true';
         		 add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
         		 add_header 'Access-Control-Allow-Headers' 'DNT,web-token,app-token,Authorization,Accept,Origin,Keep-Alive,User-Agent,X-Mx-ReqToken,X-Data-Type,X-Auth-Token,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
          		add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
          		if ($request_method = 'OPTIONS') {
            		  add_header 'Access-Control-Max-Age' 1728000;
           		   add_header 'Content-Type' 'text/plain; charset=utf-8';
           		   add_header 'Content-Length' 0;
           		   return 204;
         		 }
         		 kafka_topic tp_log_01;
      	}
   
           location / {
               root   html;
               index  index.html index.htm;
           }
   
           #error_page  404              /404.html;
   
           # redirect server error pages to the static page /50x.html
           #
           error_page   500 502 503 504  /50x.html;
           location = /50x.html {
               root   html;
           }
   
       }
   }
   ```

   

8. 让操作系统加载模块：

   ```shell
   echo "/usr/local/lib" >> /etc/ld.so.conf
   ldconfig
   ```

9. 启动Kafka

   ```shell
   zkServer.sh start
   kafka-server-start.sh /opt/kafka_2.12-1.0.2/config/server.properties
   ```

10. 启动nginx：

   ```shell
   /usr/local/nginx/sbin/nginx
   ```

11. 测试：

   ```shell
   curl localhost/log -d "hello ngx_kafka_module" -v
   ```



安装kafka集群因为配置文件导致node2节点无法启动的问题排查记录：

![image-20201220123811780](readme.assets/image-20201220123811780.png)

配置端口错误导致：

![image-20201220123907999](readme.assets/image-20201220123907999.png)