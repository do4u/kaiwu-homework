package kaiwu.do4u.flashsale.listener;

import cn.hutool.log.StaticLog;
import kaiwu.do4u.flashsale.entity.Order;
import kaiwu.do4u.flashsale.entity.OrderStatus;
import kaiwu.do4u.flashsale.service.DBService;
import kaiwu.do4u.flashsale.service.RedisService;
import kaiwu.do4u.flashsale.service.RocketMQService;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic = RocketMQService.TOPIC_PAY,
        consumerGroup = RocketMQService.GROUP_PAY,
        consumeMode = ConsumeMode.CONCURRENTLY,
        messageModel = MessageModel.CLUSTERING,
        secretKey = "*")
public class PayListener implements RocketMQListener<String> {

    @Autowired
    DBService dbService;

    @Autowired
    RedisService redisService;


    @Override
    public void onMessage(String orderId) {
        StaticLog.info("接收到支付延时取消-orderId - {}" , orderId);
        Boolean rs = dbService.updateOrder(orderId, OrderStatus.CANCEL);

        if(rs){
            redisService.incrementStock(); //取消订单恢复库存
            StaticLog.info("恢复库存+1");
        }else {
            StaticLog.warn("作废订单失败");
        }

    }
}
