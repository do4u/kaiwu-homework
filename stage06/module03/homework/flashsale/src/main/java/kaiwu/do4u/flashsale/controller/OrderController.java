package kaiwu.do4u.flashsale.controller;

import cn.hutool.core.util.RandomUtil;
import io.lettuce.core.dynamic.annotation.Param;
import kaiwu.do4u.flashsale.entity.Order;
import kaiwu.do4u.flashsale.entity.OrderStatus;
import kaiwu.do4u.flashsale.service.DBService;
import kaiwu.do4u.flashsale.service.RedisService;
import kaiwu.do4u.flashsale.service.RocketMQService;
import kaiwu.do4u.flashsale.util.JsonResult;
import kaiwu.do4u.flashsale.util.JsonResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class OrderController {

    @Autowired
    DBService dbService;

    @Autowired
    RedisService redisService;

    @Autowired
    RocketMQService rocketMQService;


    /**
     * Redis查询库存
     * @return
     */
    @RequestMapping("/stock")
    public JsonResult leftStock(){
        Integer integer = redisService.queryStock();
        return JsonResultUtil.success(integer);
    }

    /**
     *
     * @param session
     * @param isMock  压测参数，每次请求动态生成不同的userId，否则使用sessionId
     * @return
     */
    @RequestMapping("/flashBuy")
    public JsonResult flashBuy(HttpSession session , Boolean isMock){
        //todo:模拟演示生成动态userId
        String userId = "userId:" +  (isMock!=null && isMock ? RandomUtil.randomUUID() : session.getId());

        Integer stock = redisService.queryStock();
        if(stock <= 0){
            return JsonResultUtil.fail("商品已经抢购完毕");
        }

        //为抢购用户生产index序号
        Integer index = redisService.requestIndex();
        boolean lucky = redisService.isLucky(userId, index);
        if(lucky){
            //发送插入订单MQ消息，同时扣减redis中库存
            Order order = rocketMQService.sendOrder(userId, index);
            return JsonResultUtil.success(0,"抢购成功",order);
        }
        return JsonResultUtil.success(1,"排队中",index);
    }

    /**
     * Redis查询库存
     * @return
     */
    @RequestMapping("/pay")
    public JsonResult pay(String orderId){
        Boolean aBoolean = dbService.updateOrder(orderId, OrderStatus.PAYED);
        return aBoolean ? JsonResultUtil.success():JsonResultUtil.fail("支付失败");
    }


}
