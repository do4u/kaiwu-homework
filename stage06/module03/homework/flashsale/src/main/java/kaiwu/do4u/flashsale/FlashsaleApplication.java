package kaiwu.do4u.flashsale;

import kaiwu.do4u.flashsale.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FlashsaleApplication {

    @Autowired
    RedisService redisService;

    public static void main(String[] args) {
        SpringApplication.run(FlashsaleApplication.class, args);
    }

    @Bean
    public CommandLineRunner runner() {
        return args -> {
            redisService.initStock(100);//初始化库存
            redisService.initMaxStock(100);//设置最大库存值
        };
    }

}
