package kaiwu.do4u.flashsale.service;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.log.StaticLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisService {


    public final Integer MAX_STOCK_VALUE = 100;  //最大库存 默认100
    public final String MAX_STOCK = "key_max_stock";  //最大库存 默认100
    public final String STOCK = "key_stock";  //redis库存
    public final String REQUEST_INDEX = "key_request_index";  //抢购者进入序号
    public final String LUCKY_LIST = "key_lucky_list";  //抢中者列表
    public final String FRONT_TEMP_LIST = "key_temp_list";  //前面抢购中人员信息临时缓存，最多保存MAX_STOCK_VALUE

    @Autowired
    RedisTemplate<String, Integer> redisTemplate;


    /**
     * 配置的最大库存，未设置为200
     *
     * @return
     */
    public Integer getMaxStock() {
        Integer maxStock = redisTemplate.opsForValue().get(MAX_STOCK);
        return maxStock != null ? maxStock : MAX_STOCK_VALUE;
    }

    /**
     * 初始化库存数量
     *
     * @return
     */
    public void initStock(Integer stock) {
        redisTemplate.opsForValue().set(STOCK,stock);
    }

    public void initMaxStock(Integer stock) {
        redisTemplate.opsForValue().set(MAX_STOCK,stock);
    }

    /**
     * 查询库存
     *
     * @return
     */
    public Integer queryStock() {
        return redisTemplate.opsForValue().get(STOCK);
    }

    /**
     * 生成请求序号
     *
     * @return
     */
    public Integer requestIndex() {
        return Math.toIntExact(redisTemplate.opsForValue().increment(REQUEST_INDEX));
    }

    /**
     * 扣减库存
     *
     * @return
     */
    public Integer decrementStock() {
        return Math.toIntExact(redisTemplate.opsForValue().decrement(STOCK));
    }

    /**
     * 恢复redis库存
     * @return
     */
    public Integer incrementStock() {
        return Math.toIntExact(redisTemplate.opsForValue().increment(STOCK));
    }


    /**
     * 生成请求序号
     *
     * @return
     */
    public void saveLucky(String userId, Integer requestIndex) {
        redisTemplate.opsForHash().put(LUCKY_LIST, userId, requestIndex);
    }


    /**
     * 是否抢购上 默认最小概率 10%
     * 同时保存前面的用户抢购信息
     * @return
     */
    public boolean isLucky(String userId, Integer requestIndex) {
        Integer currentIndex = redisTemplate.opsForValue().get(REQUEST_INDEX);
        Integer maxStock = getMaxStock();
        Integer maxLimit;
        if (currentIndex <= maxStock) {
            //todo:如果参与秒杀人数少，100%抢购上，保存库存前需要
            StaticLog.warn("记录前N抢购用户，userId:{}，requestIndex:{}", userId, requestIndex);
            //缓存前面请求的用户信息
            redisTemplate.opsForHash().put(FRONT_TEMP_LIST, userId, requestIndex);
            maxLimit = maxStock * 10; //如果抢购人数不足库存按 10% ，即随机数最大值为最大库存2倍
        } else if (currentIndex <= maxStock * 10) {
            maxLimit = maxStock * 10; //如果抢购人数不足库存按 10% ，即随机数最大值为最大库存2倍
        } else {
            maxLimit = currentIndex;  //其他的概率按照目前参与人数来计算概率，参与人数越多，概率越低
        }
        Integer randomInt = RandomUtil.randomInt(1, maxLimit);//返回1-最大库存的随机数
        if (randomInt <= maxStock) {
            //保存抢购成功数据到redis
            saveLucky(userId,requestIndex);
            StaticLog.warn("抢购成功，userId:{}，requestIndex:{}", userId, requestIndex);
            return true;
        } else {
            StaticLog.debug("未抢购成功，userId:{}，requestIndex:{}", userId, requestIndex);
            return false;
        }
    }





}
