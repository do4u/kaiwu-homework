package kaiwu.do4u.flashsale.util;


/**
 * Created by cl on 2018/8/28.
 * 标准的JSON请求、返回结果，当不需要data属性是，仅仅使用JsonResult
 */
public class JsonResultExt<T> extends JsonResult {

    public JsonResultExt() {
    }

    public JsonResultExt(T data) {
        this.data = data;
    }

    /* 请求返回数据 */
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public JsonResultExt(int code,String message,T data) {
        super(code,message);
        this.data = data;
    }


}
