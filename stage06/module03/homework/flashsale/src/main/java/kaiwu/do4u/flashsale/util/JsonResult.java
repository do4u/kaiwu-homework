package kaiwu.do4u.flashsale.util;


/**
 * Created by cl on 2018/8/28.
 * 标准的JSON请求、返回结果
 */
public class JsonResult {

    /* 请求成功 */
    public final static int SUCCESS = 0;
    /* 请求失败 */
    public final static int FAIL = 1;

    /* 请求返回代码，标识请求成功失败 */
    private int code;
    /* 请求返回消息 */
    private String message;


    public JsonResult() {
    }

    public JsonResult(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static JsonResult success() {
        return new JsonResult(SUCCESS, "OK");
    }

    public static JsonResult error(String message) {
        return new JsonResult(FAIL, message);
    }

    public static JsonResult error(int code, String message) {
        return new JsonResult(code, message);
    }


    public boolean checkCode() {
        return getCode() == 0 ? true : false;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
