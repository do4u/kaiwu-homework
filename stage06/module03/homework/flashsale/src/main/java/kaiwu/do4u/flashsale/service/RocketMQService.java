package kaiwu.do4u.flashsale.service;

import cn.hutool.log.StaticLog;
import kaiwu.do4u.flashsale.entity.Order;
import kaiwu.do4u.flashsale.entity.OrderStatus;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RocketMQService {

    public final static String TOPIC_SEND_ORDER = "tp_flashsale";
    public final static String GROUP_ORDER = "grp_flashsale";

    public final static String TOPIC_PAY = "tp_flashpay";
    public final static String GROUP_PAY = "grp_pay";

    @Autowired
    RedisService redisService;

    @Autowired
    RocketMQTemplate rocketMQTemplate;


    public Order sendOrder(String userId, Integer index){
        Order order = new Order();
        order.setOrderId(UUID.randomUUID().toString());
        order.setOrderStatus(OrderStatus.NEW);
        order.setUserId(userId);
        order.setContent("抢购商品");
        order.setFlashIndex(index);
        rocketMQTemplate.asyncSend(TOPIC_SEND_ORDER, order, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                //扣钱库存
                redisService.decrementStock();
                StaticLog.info("发送创建订单消息："+order);
            }

            @Override
            public void onException(Throwable throwable) {
                throwable.printStackTrace();
                StaticLog.error("消息队列异常",throwable);
            }
        });
        return order;
    }
}
