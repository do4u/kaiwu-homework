package kaiwu.do4u.flashsale.entity;

public enum OrderStatus {

    NEW(0),PAYED(1),CANCEL(2);

    private int status;

    OrderStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return this.status;
    }

}
