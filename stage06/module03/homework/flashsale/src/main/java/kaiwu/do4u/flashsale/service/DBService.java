package kaiwu.do4u.flashsale.service;

import kaiwu.do4u.flashsale.entity.Order;
import kaiwu.do4u.flashsale.entity.OrderStatus;
import kaiwu.do4u.flashsale.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class DBService {

    @Autowired
    OrderRepository orderRepository;


    /**
     * 创建新订单
     * @param order
     */
    @Transactional
    public void saveOrder(Order order){
        orderRepository.save(order);
    }

    /**
     * 根据订单id更新订单状态
     * @param orderId
     * @param status
     */
    @Transactional
    public Boolean updateOrder(String orderId, OrderStatus status){
        Optional<Order> optional = orderRepository.findById(orderId);
        Order order = optional.get();
        if(order != null){
            order.setOrderStatus(status);
            Order save = orderRepository.save(order);
            return save!=null?true:false;
        }
        return false;
    }

}
