package kaiwu.do4u.flashsale.listener;

import cn.hutool.log.StaticLog;
import kaiwu.do4u.flashsale.entity.Order;
import kaiwu.do4u.flashsale.service.DBService;
import kaiwu.do4u.flashsale.service.RocketMQService;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic = RocketMQService.TOPIC_SEND_ORDER,
        consumerGroup = RocketMQService.GROUP_ORDER,
        consumeMode = ConsumeMode.CONCURRENTLY,
        messageModel = MessageModel.CLUSTERING,
        secretKey = "*")
public class OrderListener implements RocketMQListener<Order> {

    @Autowired
    DBService dbService;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Override
    public void onMessage(Order order) {
        System.out.println(order.getOrderStatus().getStatus());
        StaticLog.info("向数据库插入记录-service - {}" , order);

        dbService.saveOrder(order);

        Message message = new Message(RocketMQService.TOPIC_PAY, order.getOrderId().getBytes());

        org.springframework.messaging.Message springMessage = RocketMQUtil.convertToSpringMessage(message);

        rocketMQTemplate.asyncSend(message.getTopic(), springMessage, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                StaticLog.info("发送支付延迟取消消息成功："+sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                throwable.printStackTrace();
                StaticLog.error("消息队列异常",throwable);
            }
        }, 3000, 3); // 3 为 10s ： 默认值为“1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m  9m 10m 20m 30m 1h 2h”，18个level。

    }
}
