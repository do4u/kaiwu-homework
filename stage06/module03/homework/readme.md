# 秒杀场景



秒杀设计：

1、/flashBuy 接口

首先判断redis库存是否充足，库存不足返回抢购失败，否则进入下一步

生成redis中抢购序号，根据动态概率判断是否抢购成功，成功后异步发送orderMQ订单消息，接口返回抢购成功；未成功抢购返回抢购序号，等待轮询抢购结果



2、消息队列 orderMQ

接收到消息，向数据表保存订单信息，然后向payMQ发送一个延时消费的消息，用于超时支付取消订单恢复库存



3、消息队列payMQ

接收到消息，更新订单状态，恢复redis中库存数



4、启动地址：http://127.0.0.1:8080/

![image-20201216040656078](./rocketMQHomeWork.assets/image-20201216041554408.png)

```
2020-12-16 04:02:43.285  INFO 27496 --- [MessageThread_8] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - eb00647c-724e-45ea-b27b-d3dc71fb327c
2020-12-16 04:02:43.585  INFO 27496 --- [MessageThread_8] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:02:44.100  INFO 27496 --- [MessageThread_9] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - 01f47440-c7e1-4d25-b3c5-022290959ab4
2020-12-16 04:02:44.133  WARN 27496 --- [nio-8080-exec-5] k.do4u.flashsale.service.RedisService    : 抢购成功，userId:userId:dae6d8af-0329-49ba-b9c9-2d7c9a0c4c55，requestIndex:1034
0
2020-12-16 04:02:44.142  INFO 27496 --- [essageThread_15] k.do4u.flashsale.listener.OrderListener  : 向数据库插入记录-service - Order{orderId='4b1ace56-2bec-4182-a46e-67cf39c5f1dd', userId='userId:dae6d8af-0329-49ba-b9c9-2d7c9a0c4c55', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:44.200  INFO 27496 --- [ublicExecutor_3] k.d.flashsale.service.RocketMQService$1  : 发送创建订单消息：Order{orderId='4b1ace56-2bec-4182-a46e-67cf39c5f1dd', userId='userId:dae6d8af-0329-49ba-b9c9-2d7c9a0c4c55', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:44.438  INFO 27496 --- [MessageThread_9] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:02:44.508  WARN 27496 --- [nio-8080-exec-3] k.do4u.flashsale.service.RedisService    : 抢购成功，userId:userId:d2f844d4-16d7-4031-a5a8-36c2a694e238，requestIndex:1035
2020-12-16 04:02:44.512  INFO 27496 --- [ublicExecutor_4] k.d.flashsale.listener.OrderListener$1   : 发送支付延迟取消消息成功：SendResult [sendStatus=SEND_OK, msgId=C0A808216B6818B4AAC24E1DA09C014F, offsetMsgId=C0A8082100002A9F0000000000016683, messageQueue=MessageQueue [topic=tp_flashpay, brokerName=LAPTOP-2CJ704G5, queueId=3], queueOffset=50]
0
2020-12-16 04:02:44.518  INFO 27496 --- [essageThread_16] k.do4u.flashsale.listener.OrderListener  : 向数据库插入记录-service - Order{orderId='1d1ed236-001a-4068-8192-ac354fe0daa6', userId='userId:d2f844d4-16d7-4031-a5a8-36c2a694e238', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:44.544  INFO 27496 --- [ublicExecutor_5] k.d.flashsale.service.RocketMQService$1  : 发送创建订单消息：Order{orderId='1d1ed236-001a-4068-8192-ac354fe0daa6', userId='userId:d2f844d4-16d7-4031-a5a8-36c2a694e238', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:44.818  INFO 27496 --- [ublicExecutor_1] k.d.flashsale.listener.OrderListener$1   : 发送支付延迟取消消息成功：SendResult [sendStatus=SEND_OK, msgId=C0A808216B6818B4AAC24E1DA1CF0157, offsetMsgId=C0A8082100002A9F0000000000016C5E, messageQueue=MessageQueue [topic=tp_flashpay, brokerName=LAPTOP-2CJ704G5, queueId=1], queueOffset=51]
2020-12-16 04:02:45.328  WARN 27496 --- [nio-8080-exec-8] k.do4u.flashsale.service.RedisService    : 抢购成功，userId:userId:b5bfe834-7aca-417d-b418-93d945568f35，requestIndex:1038
0
2020-12-16 04:02:45.347  INFO 27496 --- [essageThread_17] k.do4u.flashsale.listener.OrderListener  : 向数据库插入记录-service - Order{orderId='a2e0e817-36f1-4906-a767-a84b2189e609', userId='userId:b5bfe834-7aca-417d-b418-93d945568f35', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:45.429  INFO 27496 --- [ublicExecutor_6] k.d.flashsale.service.RocketMQService$1  : 发送创建订单消息：Order{orderId='a2e0e817-36f1-4906-a767-a84b2189e609', userId='userId:b5bfe834-7aca-417d-b418-93d945568f35', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:45.672  INFO 27496 --- [ublicExecutor_2] k.d.flashsale.listener.OrderListener$1   : 发送支付延迟取消消息成功：SendResult [sendStatus=SEND_OK, msgId=C0A808216B6818B4AAC24E1DA526015F, offsetMsgId=C0A8082100002A9F000000000001723A, messageQueue=MessageQueue [topic=tp_flashpay, brokerName=LAPTOP-2CJ704G5, queueId=0], queueOffset=52]
2020-12-16 04:02:45.705  INFO 27496 --- [essageThread_10] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - a8e4fb5c-c449-47da-a580-d787b4896670
2020-12-16 04:02:45.948  INFO 27496 --- [essageThread_11] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - af0f10cc-3121-4ebe-9ccb-5a58aec419d6
2020-12-16 04:02:46.044  INFO 27496 --- [essageThread_10] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:02:46.260  INFO 27496 --- [essageThread_12] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - d276e2ab-9cb0-4b75-88af-1e04f0057143
2020-12-16 04:02:46.283  INFO 27496 --- [essageThread_11] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:02:46.667  INFO 27496 --- [essageThread_12] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:02:48.794  INFO 27496 --- [essageThread_13] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - 2ccab1fa-69d8-46ed-b312-84319a0907f1
2020-12-16 04:02:49.156  INFO 27496 --- [essageThread_13] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:02:50.109  INFO 27496 --- [essageThread_14] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - 95547c1a-e0cc-47c5-8c8e-d052b55a022b
2020-12-16 04:02:50.377  WARN 27496 --- [io-8080-exec-10] k.do4u.flashsale.service.RedisService    : 抢购成功，userId:userId:cb9ad5d5-9556-42ff-8218-833def1a859c，requestIndex:1057
0
2020-12-16 04:02:50.383  INFO 27496 --- [essageThread_18] k.do4u.flashsale.listener.OrderListener  : 向数据库插入记录-service - Order{orderId='c2d6948e-60fa-48ff-8b72-0630150cd906', userId='userId:cb9ad5d5-9556-42ff-8218-833def1a859c', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:50.446  INFO 27496 --- [ublicExecutor_3] k.d.flashsale.service.RocketMQService$1  : 发送创建订单消息：Order{orderId='c2d6948e-60fa-48ff-8b72-0630150cd906', userId='userId:cb9ad5d5-9556-42ff-8218-833def1a859c', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:50.477  INFO 27496 --- [essageThread_14] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:02:50.789  INFO 27496 --- [ublicExecutor_4] k.d.flashsale.listener.OrderListener$1   : 发送支付延迟取消消息成功：SendResult [sendStatus=SEND_OK, msgId=C0A808216B6818B4AAC24E1DB9230171, offsetMsgId=C0A8082100002A9F0000000000017EB9, messageQueue=MessageQueue [topic=tp_flashpay, brokerName=LAPTOP-2CJ704G5, queueId=0], queueOffset=53]
2020-12-16 04:02:52.300  WARN 27496 --- [nio-8080-exec-4] k.do4u.flashsale.service.RedisService    : 抢购成功，userId:userId:e2e22370-adc3-4f84-810f-d17fc63b92f6，requestIndex:1062
0
2020-12-16 04:02:52.305  INFO 27496 --- [essageThread_19] k.do4u.flashsale.listener.OrderListener  : 向数据库插入记录-service - Order{orderId='4e1cfcdf-e5f8-4261-8c8c-6ce814a520af', userId='userId:e2e22370-adc3-4f84-810f-d17fc63b92f6', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:52.397  INFO 27496 --- [ublicExecutor_5] k.d.flashsale.service.RocketMQService$1  : 发送创建订单消息：Order{orderId='4e1cfcdf-e5f8-4261-8c8c-6ce814a520af', userId='userId:e2e22370-adc3-4f84-810f-d17fc63b92f6', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:52.717  INFO 27496 --- [ublicExecutor_1] k.d.flashsale.listener.OrderListener$1   : 发送支付延迟取消消息成功：SendResult [sendStatus=SEND_OK, msgId=C0A808216B6818B4AAC24E1DC0A80179, offsetMsgId=C0A8082100002A9F0000000000018494, messageQueue=MessageQueue [topic=tp_flashpay, brokerName=LAPTOP-2CJ704G5, queueId=2], queueOffset=54]
2020-12-16 04:02:52.934  WARN 27496 --- [nio-8080-exec-3] k.do4u.flashsale.service.RedisService    : 抢购成功，userId:userId:0a88e847-0d93-46fe-b757-2ea6f1705675，requestIndex:1064
0
2020-12-16 04:02:52.946  INFO 27496 --- [essageThread_20] k.do4u.flashsale.listener.OrderListener  : 向数据库插入记录-service - Order{orderId='62f9ddb3-30a9-4244-8883-946da8153c20', userId='userId:0a88e847-0d93-46fe-b757-2ea6f1705675', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:53.088  INFO 27496 --- [ublicExecutor_6] k.d.flashsale.service.RocketMQService$1  : 发送创建订单消息：Order{orderId='62f9ddb3-30a9-4244-8883-946da8153c20', userId='userId:0a88e847-0d93-46fe-b757-2ea6f1705675', orderStatus=NEW, content='抢购商品'}
2020-12-16 04:02:53.359  INFO 27496 --- [ublicExecutor_2] k.d.flashsale.listener.OrderListener$1   : 发送支付延迟取消消息成功：SendResult [sendStatus=SEND_OK, msgId=C0A808216B6818B4AAC24E1DC3270181, offsetMsgId=C0A8082100002A9F0000000000018A6F, messageQueue=MessageQueue [topic=tp_flashpay, brokerName=LAPTOP-2CJ704G5, queueId=1], queueOffset=55]
2020-12-16 04:02:54.513  INFO 27496 --- [essageThread_15] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - 4b1ace56-2bec-4182-a46e-67cf39c5f1dd
2020-12-16 04:02:54.821  INFO 27496 --- [essageThread_16] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - 1d1ed236-001a-4068-8192-ac354fe0daa6
2020-12-16 04:02:54.983  INFO 27496 --- [essageThread_15] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:02:55.260  INFO 27496 --- [essageThread_16] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:02:55.679  INFO 27496 --- [essageThread_17] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - a2e0e817-36f1-4906-a767-a84b2189e609
2020-12-16 04:02:56.081  INFO 27496 --- [essageThread_17] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:03:00.792  INFO 27496 --- [essageThread_18] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - c2d6948e-60fa-48ff-8b72-0630150cd906
2020-12-16 04:03:01.136  INFO 27496 --- [essageThread_18] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:03:02.719  INFO 27496 --- [essageThread_19] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - 4e1cfcdf-e5f8-4261-8c8c-6ce814a520af
2020-12-16 04:03:03.143  INFO 27496 --- [essageThread_19] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
2020-12-16 04:03:03.357  INFO 27496 --- [essageThread_20] k.do4u.flashsale.listener.PayListener    : 接收到支付延时取消-orderId - 62f9ddb3-30a9-4244-8883-946da8153c20
2020-12-16 04:03:03.687  INFO 27496 --- [essageThread_20] k.do4u.flashsale.listener.PayListener    : 恢复库存+1
```

