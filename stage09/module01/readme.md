# Docker作业

（1）Hot是应用程序(springboot)，打成jar包：Hot.jar 

（2）利用dockerfile将Hot.jar构建成镜像lgedu/hot:1.0 

（3）构建Swarm 集群

（4）在 Swarm 集群中使用 compose 文件 （docker-compose.yml） 来配置、启动多个服务 包括: Mysql、Redis以及应用程序Hot



## 答题

### 实验主机：

| ip地址        | 主机名 | 备注    |
| ------------- | ------ | ------- |
| 192.168.3.102 | node2  | manager |
| 192.168.3.103 | node3  | worker  |
| 192.168.3.104 | node4  | worker  |

1、安装docker

```
#查询docker是否安装
rpm -qa|grep docker
dokcer -v

#未安装安装docker
yum install -y docker-ce docker-ce-cli containerd.io
```



2、拉取需要的镜像，node2，node3 和 node4上分别执行

```sh
docker pull swarm
#查看swarm版本
docker run -it --rm swarm -v
```

3、加入swarm集群

```
docker swarm init --advertise-addr 192.168.3.102
#根据返回的token，在node3，node4上执行
docker swarm join --token SWMTKN-1-1ujd4h34xjjjx98aglfcxp0e7fo82yeayzfixvv5y0z6w598mc-1vf5hn6yekxlr4wi48qh7lfqn 192.168.3.102:2377

#查看swarm集群
docker node ls
docker service ls
```

4、拉取集群，建立centos-jdk版本镜像

```sh
#拉取镜像
docker pull centos:7.6.1810
docker pull redis:5.0.9
docker pull mysql:5.7.30 

#启动centos官方镜像，安装jdk，本机/home/icy的rpm包
docker run -it --rm -d -v /home/icy:/app centos:7.6.1810 /bin/bash

#进入容器内部
docker exec -it 58 /bin/bash
cd /app
#安装jdk
rpm -ivh jdk-8u261-linux-x64.rpm
#退出容器
exit

#查看运行容器id
docker ps 
#保存安装jdk后的centos镜像
docker commit 583 lgedu/centos-jdk8:1.0

#查看镜像列表
docker images
```

5、根据Dockerfile创建lgedu/hot:1.0镜像

本地打包springboot项目为jar，重命名为hot.jar，并上传服务器

```yml
#Dockerfile文件
From lgedu/centos-jdk8:1.0
COPY ./hot.jar /home/hot.jar
WORKDIR /home
EXPOSE 8080:8080
CMD [ "java","-jar", "hot.jar" ]

#进入Dokerfile当前文件夹，创建镜像
docker build -t lgedu/hot:1.0 .
#查看镜像列表
docker images
```

6、编辑docker-compose.yml文件

```yml
version: "3.0"
services:
  springboot:
    image: lgedu/hot:1.0
    ports:
      - 8080:8080
    deploy:
      mode: replicated
      replicas: 2
  mysql:
    image: mysql:5.7.30
    ports:
      - 3306:3306
    command:
      --default-authentication-plugin=mysql_native_password
      --character-set-server=utf8mb4
      --collation-server=utf8mb4_general_ci
      --explicit_defaults_for_timestamp=true
      --lower_case_table_names=1
      --default-time-zone=+8:00
    environment:
      MYSQL_ROOT_PASSWORD: "root"
    volumes:
      - "/docker/mysql/db:/var/lib/mysql"
    deploy:
      mode: replicated
      replicas: 2
  redis:
    image: redis:5.0.9
    environment:
      - TZ=Asia/beijing
    ports:
      - 16379:6379 #主备都存在
    volumes:
      - /docker/redis/data:/data
    deploy:
      mode: replicated
      replicas: 2
```

7、启动swarm集群镜像

```sh
#三台主机分别创建本机挂在目录
mkdir -p /docker/mysql/db
mkdir -p /docker/redis/data

#进入docker—compose.yml目录，启动集群应用
docker stack deploy -c docker-compose.yml web

#查看启动情况
docker service ls
```



springboot代码：https://gitee.com/do4u/kaiwu-homework/tree/master/stage09/module01/hot

演示视频：https://gitee.com/do4u/kaiwu-homework/blob/master/stage09/module01/swarm_compose_02.wmv

