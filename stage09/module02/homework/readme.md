# 作业：Mysql服务安装、springboot项目部署、浏览器客户端访问调用



（1）搭建mysql服务器，要求必须要挂载数据卷，挂载方式不限。可以使用deployment或者statefulSet方式部署

（2）将项目数据库导入mysql服务器。可以采用sqlyog、navicat。

（3）开发spirnboot项目。

（4）spirnboot项目打包测试。

（5）spirnboot项目制作成镜像。

（6）在k8s集群中spirnboot项目。

考察技能点：

（1）mysql数据卷挂载。deployment的yml文件编写。

（2）java项目基础开发能力

（3）springboot项目至少要完成一张表查询所有记录的功能。推荐完成单表的CRUD。可以使用postman等工具进行测试。不需要编写页面。

（4）Dockerfile基本技能

（5）mysql容器与springboot项目容器相互访问模式：以下方式任选其一。

1. 采用SVC的IP地址方式
2. 采用SVC的名称方式 



 另：作业需要提交演示视频



## 答题

### 一、系统准备，采用centos7.9（7.8已经没有下载地址）

#### 1、升级系统内核至 4.4

```sh
yum intall -y wget

#备份安装yum安装源
mv /etc/yum.repos.d /etc/yum.repos.d.backup

#下载源配置文件到指定文件
wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
wget -O /etc/yum.repos.d/epel.repo https://mirrors.aliyun.com/repo/epel-7.repo

#更新软件
yum clean all
yum makecache
yum repolist
yum update

#下载并安装内核4.4,其中7.0-3.el7 l不是数字1，而是字母L的小写
rpm -Uvh https://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm
yum --enablerepo=elrepo-kernel install -y kernel-lt
 
grep initrd16 /boot/grub2/grub.cfg
#根据多个内核的顺序配置默认启动选项
#返回
#	initrd16 /initramfs-4.4.248-1.el7.elrepo.x86_64.img
#	initrd16 /initramfs-3.10.0-1160.11.1.el7.x86_64.img
#	initrd16 /initramfs-3.10.0-1160.el7.x86_64.img
#	initrd16 /initramfs-0-rescue-66b0d5a4a6764b9ea674e2ac063e4c1a.img
grub2-set-default 0

#或者通过  cat /boot/grub2/grub.cfg |grep menuentry  根据名称设置启动项
#grub2-set-default 'CentOS Linux (4.4.248-1.el7.elrepo.x86_64) 7 (Core)'
```

#### 2、配置其他内容

```sh
#关闭防火墙
systemctl stop firewalld
systemctl disable firewalld

#关闭selinux
sed -i 's/SELINUX=enforcing/SELINUX=disable/g' /etc/sysconfig/selinux
setenforce 0
#确认关闭配置信息
cat /etc/sysconfig/selinux

#配置网桥过滤
vi /etc/sysctl.conf
#文件末尾添加下列内容
net.bridge.bridge-nf-call-ip6tables=1
net.bridge.bridge-nf-call-iptables=1
net.bridge.bridge-nf-call-arptables=1
net.ipv4.ip_forward=1
net.ipv4.ip_forwar_use_pmtu=0
#生效配置
sysctl --system
#查看配置结果
sysctl -a |grep "ip_forward"

#安装配置IPVS，讲义中ipvadm不存在，可能是ipvsadm
yum install ipset ipvsadm
vi /etc/sysconfig/modules/ipvs.modules
#添加一下内容
#!/bin/bash
modprobe -- ip_vs
modprobe -- ip_vs_rr
modprobe -- ip_vs_wrr
modprobe -- ip_vs_sh
modprobe -- nf_conntrack_ipv4
#设置执行权限
chmod 755 /etc/sysconfig/modules/ipvs.modules
#执行脚本
bash /etc/sysconfig/modules/ipvs.modules
#确认配置结果
lsmod |grep -e ip_vs -e nf_conntrack_ipv4

#同步确认时区和时间
yum -y install ntpdate
ntpdate time1.aliyun.com
rm -rf /etc/localtime 
ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

#命令补全工具
yum install -y bash-completion bash-completion-extras
source /etc/profile.d/bash_completion.sh 

#关闭swap分区 临时关闭：swapoff -a
vi /etc/fstab
#注释掉：/dev/mapper/centos-swap swap swap defaults 0 0
free -m

#hosts文件
vi /etc/hosts
#添加下面内容
192.168.3.200 k8s-master01
192.168.3.201 k8s-node01
192.168.3.202 k8s-node02
192.168.3.203 k8s-node03

#重新生效配置
reboot
```



### 二、Dokcer及k8s安装

#### 1、注册阿里云镜像服务，添加本地docker镜像

![image-20201226183627629](readme.assets/image-20201226183627629.png)

```bash
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://2eza1yr4.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

#### 2、安装docker

```sh
#安装依赖
yum install -y yum-utils device-mapper-persistent-data lvm2

#配置安装源
yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

#安装docker最新版本
yum install -y docker-ce
#查询docker可使用版本
yum list docker-ce.x86_64 --showduplicates|sort -r
systemctl enable docker
systemctl start docker


#设置cgroup driver
vi /etc/docker/daemon.json
#添加一下内容到json配置文件根节点
"exec-opts": ["native.cgroupdriver=systemd"]

#重新加载docker
systemctl daemon-reload
systemctl restart docker


```





#### 3、配置k8s安装源

```sh
#编辑源
vi /etc/yum.repos.d/kubernetes.repo

##添加文件内容
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
       https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
       
#更新缓存
yum clean all
yum -y makecache
```

#### 5、安装kubeadm

```sh
#查询最新版本
yum list |grep kubeadm
#查询所有版本
yum list kubelet --showduplicates | sort -r

#安装对应版本
yum install -y kubelet-1.17.5 kubeadm-1.17.5 kubectl-1.17.5

#启动kubelet
systemctl enable kubelet
systemctl start kubelet
#查看版本
kubelet --version

#设置cgroup driver，同docker保持一致
vi /etc/sysconfig/kubelet 
KUBELET_EXTRA_ARGS="--cgroup-driver=systemd"
```



#### 6、通过脚本下载阿里云镜像，重命名为k8s.gcr.io镜像并导出

```sh
#!/bin/bash 
# 下面的镜像应该去除"k8s.gcr.io"的前缀，版本换成kubeadm config images list命令获取 到的版本 
images=( 
	kube-apiserver:v1.17.5
	kube-controller-manager:v1.17.5
	kube-scheduler:v1.17.5
	kube-proxy:v1.17.5
	pause:3.1
	etcd:3.4.3-0
	coredns:1.6.5
)
for imageName in ${images[@]} ; 
do 
	docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName 
	docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName k8s.gcr.io/$imageName 
	docker rmi registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName 
done
```

#### 7、导出镜像为tar文件并导入

```bash
docker save -o k8s.1.17.5.tar \
k8s.gcr.io/kube-proxy:v1.17.5 \
k8s.gcr.io/kube-apiserver:v1.17.5 \
k8s.gcr.io/kube-controller-manager:v1.17.5 \
k8s.gcr.io/kube-scheduler:v1.17.5 \
k8s.gcr.io/coredns:1.6.5 \
k8s.gcr.io/etcd:3.4.3-0 \
k8s.gcr.io/pause:3.1

#master节点需要全部镜像
docker load -i k8s.1.17.5.tar


docker save -o k8s.1.17.5.node.tar \
k8s.gcr.io/kube-proxy:v1.17.5 \
k8s.gcr.io/pause:3.1

#通过scp传递镜像包
scp k8s.1.17.5.tar 192.168.3.200:/data

#node节点需要kube-proxy:v1.17.5和pause:3.1,2个镜像 
docker load -i k8s.1.17.5.node.tar
```



### 三、k8s集群配置

#### 1、配置主机名：

```
hostnamectl set-hostname k8s-master01
hostnamectl set-hostname k8s-node01
hostnamectl set-hostname k8s-node02
hostnamectl set-hostname k8s-node03
```

#### 2、初始化集群,，在master节点执行：

```sh
kubeadm init --apiserver-advertise-address=192.168.3.200 --kubernetes-version v1.17.5 --service-cidr=10.10.0.0/16 --pod-network-cidr=10.8.0.0/16
```

返回结果中：master节点执行：

```sh
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

返回结果中：三个node节点执行：

```sh
kubeadm join 192.168.3.200:6443 --token leb04t.6fuxdgdfsdin1rx2 \
    --discovery-token-ca-cert-hash sha256:51bb9faa268aad836c40706d111be04226808fad8cdaed16537a1e19ec4148e4
```

#### 3、启动calico.yml：

```sh
#加载配置文件
mkdir -p /data
cd /data
wget https://docs.projectcalico.org/v3.14/manifests/calico.yaml

#预先下载docker镜像
docker pull calico/cni:v3.14.2 &&
docker pull calico/pod2daemon-flexvol:v3.14.2 &&
docker pull calico/node:v3.14.2 &&
docker pull calico/kube-controllers:v3.14.2

#查看节点状态
kubectl get nodes
#安装部署k8s集群
kubectl apply -f calico.yaml

#监控节点状态，直至完全部署成功
kubectl get nodes -w
```

#### 4、自动补全kebectl命令

```shell
#确认是否安装依赖包
yum info bash-completion

echo "source <(kubectl completion bash)" >> ~/.bash_profile
source ~/.bash_profile
```

### 四、部署springboot项目